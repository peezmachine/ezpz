

EzPz is a grab bag of things I've made to support my Unity3D projects, but nothing here is Unity-specific.
However, I've implemented certain features (like obsvervable collections) that are present in newer versions
of .NET but not in the older ~3.5-ish feature set of Unity's version of Mono.

We got:

- Messaging stuff (publish-subscribe)
- Collections (observable; fast-lookup/remove linked list)
- Probably more, since I guarantee I will forget to update this readme.

