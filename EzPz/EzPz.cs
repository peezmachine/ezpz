﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.CompilerServices;
using System.Collections.ObjectModel;
using EzPz.Collections;


namespace EzPz{

	public class CollectionChangedEventArgs<T> {
		public enum EventType {Add, Remove, Clear};

		EventType _type;
		IList<T> _newItems;
		IList<T> _oldItems;

		public EventType Type { get { return _type; } }
		public IList<T> NewItems { get { return _newItems; } }
		public IList<T> OldItems { get { return _oldItems; } }

		public CollectionChangedEventArgs (EventType type, IList<T> newItems, IList<T> oldItems){
			_type = type;
			_newItems = newItems;
			_oldItems = oldItems;
		}

	}

	public delegate void CollectionChangedEventHandler<T>(CollectionChangedEventArgs<T> args);

	//IObservableCollection is pretty much just a minor INotifyCollectionChanged replacement 
	//Since EzPz was created to support Unity3D projects and Unity's Mono version did
	//not support that feature at the time.
	public interface IObservableCollection<T> : ICollection<T> {
		event CollectionChangedEventHandler<T> OnCollectionChange;


	}
		
}






