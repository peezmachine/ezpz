﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using EzPz.Collections;


namespace EzPz.Messaging {


    //Not 100% sure about naming this IMessageQueue since at the moment there's nothing that 
    //intrisically ties it to messages, but it's possible this interface will expand to have
    //more message-centric methods, so let's keep it and see what happens!
    //public interface IMessageQueue<TMessage> {
    //	void Enqueue (TMessage message, Action callback);
    //}

    /*
    Message interface that supports async blocking
     implementation will vary, but in general expect blockers to need to be UNIQUE
     This system is somewhat crude and does not provide thread-safe locking, merely
     a way of communicating if and when a message has been fully processed by all handlers

     Note: the message processor itself should register as a blocker while it is sending
     the message so that event don't risk firing if a handler quickly adds and removes
     a blocker during the Process call.The IMessage does not expose a method for manually
     invoking the callback, so assume that it will fire once the last blocker is removed.

    The common use case would be to only have a single blocker at a time, with other
    handles listening to OnUnblock and then claiming a block if that event fires
    and it's unblocked when it gets handled.


        EXAMPLE:

        void SomeHandler(Message m){
            if (m.IsBlocked){
                m.OnUnblock += delegate { if(!m.IsBlocked) CoolHelper(m);};

                return;
            } 

        void CoolHelper(Message m) {
            var blocker = new object();
            m.AddBlocker(blocker);

            // do your thing here...

            m.RemoveBlocker(blocker);

        }

     */


    public interface IMessage {
        bool IsBlocked { get; } // a block is anything that hasn't finished dealing with the message
        void AddBlocker(object blocker);
        void RemoveBlocker(object blocker);
        event Action OnUnblock; // called whenever a blocker is removed
        event Action OnFinish;  // called after a blocker is removed if nobody else immediately claims a block
    }

    /*
     * An IMessage implemenation that also tracks a causality chain of "parents"
     * 
     * Note: We are expecting the OnUnblock/UnFinish events to fire listeners in the order
     * they are registered. This seems to be the case, but if it's too much of a reliance
     * on an implementation detail, we can make it explicit by simply backing the public
     * event with a NodeList of listeners.
     */ 
    public class Message : IMessage {
        public bool IsBlocked { get; private set; }
        HashSet<object> Blockers;  

        ICollection<Message> Parents;

        
        public event Action OnUnblock = delegate { };
        public event Action OnFinish = delegate { };

        public Message(Message parent = null) {
            if (parent != null) {
                NodeList<Message> parents = new NodeList<Message>(parent.Parents);
                parents.Add(parent, NodeList.KeyNode.Head);
                this.Parents = parents;
            } else {
                this.Parents = new NodeList<Message>();

            }
            this.Blockers = new HashSet<object>();
        }


        public void AddBlocker(object blocker) {
            if (blocker == null)
                return;
            this.Blockers.Add(blocker);
            this.IsBlocked = true;
        }

        public void RemoveBlocker(object blocker) {
            if (!this.Blockers.Remove(blocker))
                return; // abort if we tried to remove non-existent blocker
            this.IsBlocked = (this.Blockers.Count > 0);
            if (!this.IsBlocked) {
                this.OnUnblock.Invoke();
                //this next bit might look goofy, but we're seeing if anybody who responded to
                //OnUnblock went and blocked it.
                if (!this.IsBlocked)
                    this.OnFinish.Invoke();
            }
        }
    }



    /*
     * 
     * To support async, we need the Processor to only fire the callback
     * post-send if there are no blockers -- else it needs to let the blockers handle it.
     * This places a restriction on the TMessage types -- they need to have a blocker field,
     * hence the IMessage requirement
     * 
     * 
     */



    public interface IMessageProcessor<in TMessage> where TMessage : IMessage {
        void FireMessage(TMessage message);
        void EnqueueMessage(TMessage message);

    }

    //MessageProcessor interface: topic-sorted, can provide multiple topics manually
    public interface IMessageProcessor<in TMessage, TChannelKey> {
        void FireMessage(TMessage message, params TChannelKey[] channelKey);
        void EnqueueMessage(TMessage message, params TChannelKey[] channelKey);
    }









    //	
    //  The 2-generic ISubscriptionBroker interface facilitaces content-based filtering.
    //  For example, it could distribute a very general "Thing A happend" message to a signal
    //  for when Thing A happens to Subject B 
    //
    //	}

    public interface ISubscriptionBroker<TMessage, in TFilter> {


        void Subscribe(Action<TMessage> subscriber, TFilter filter);
        void Unsubscribe(Action<TMessage> subscriber, TFilter filter);
    }

    /*
     * The single-generic version supports single-channel and "catch-all" behavior
     * 
     */
    public interface ISubscriptionBroker<TMessage> {
        void Subscribe(Action<TMessage> subscriber);
        void Unsubscribe(Action<TMessage> subscriber);

    }

    ////Just a combo interface
    //public interface IMultiModeSubscriptionBroker<TMessasge, in TFilter> : ISubscriptionBroker<TMessasge, TFilter>, ISubscriptionBroker<TMessasge> { }



    public interface IChannel<T> {
        ICollection<Action<T>> Subscribers { get; }
        ReadOnlyCollection<IChannel<T>> SubChannels {get;} 

		void AddSubChannel(IChannel<T> channel);
		void AddSuperChannel(IChannel<T> channel);
		void RemoveSubChannel(IChannel<T> channel);
		void RemoveSuperChannel (IChannel<T> channel);


	}




    /*
     * A topic-filtered message processor. 
     * All messages go to a single queue, but are distributed via keyed channels
     * 
     * TODO -- maybe implement catch-all functionality (mainly for logging?)
     *  via an observable MessageFiredSignal? 
     * 
     */ 
    public class MessageProcessor<TMessage,  TChannelKey> : IMessageProcessor<TMessage, TChannelKey> where TMessage : IMessage {




        Queue<TMessage> _queue;
        IDictionary<TChannelKey, ICollection<Action<TMessage>>> _channelMap;


        public MessageProcessor(Queue<TMessage> queue,  IDictionary<TChannelKey, ICollection<Action<TMessage>>> channelMap) {
            _queue = queue;
            _channelMap = channelMap;
        }

        public void FireMessage(TMessage message, params TChannelKey[] channelKeys ) {
            message.AddBlocker(this);
            ICollection<Action<TMessage>> channel;
            foreach (TChannelKey channelKey in channelKeys) {
                if (_channelMap.TryGetValue(channelKey, out channel)) {
                    foreach (Action<TMessage> subscriber in channel) {
                        subscriber.Invoke(message);
                    }
                }
            }
            message.RemoveBlocker(this);
        }

        public void EnqueueMessage(TMessage message, params TChannelKey[] channelKeys) {
            _queue.Enqueue(message);

        }
    }


    /*
 * A TOPIC-BASED processor that auto-selects topic(s) based on CONTENT-based filtering. Wild, I know.
 * The most obvious use for this is as the base class for a message processor that automatically distributes
 * messages based on their run-time type.
 *
 * 
 * TODO -- maybe implement catch-all functionality (mainly for logging?)
 *  via an observable MessageFiredSignal? 
 * 
 */
    public class AutoChannelMessageProcessor<TMessage, TChannelKey> : IMessageProcessor<TMessage> where TMessage:IMessage {




        Queue<TMessage> _queue;
        IDictionary<TChannelKey, ICollection<Action<TMessage>>> _channelMap;
        protected Func<TMessage, IEnumerable<TChannelKey>> Sorter;


        public AutoChannelMessageProcessor(Queue<TMessage> queue, IDictionary<TChannelKey, ICollection<Action<TMessage>>> channelMap, Func<TMessage, IEnumerable<TChannelKey>> sorter) {
            _queue = queue;
            _channelMap = channelMap;
            this.Sorter = sorter;
        }


        //uses the sorter to find target channel keys!
        public void FireMessage(TMessage message) {
            message.AddBlocker(this);
            foreach(TChannelKey channelKey in this.Sorter.Invoke(message)) {
                ICollection<Action<TMessage>> channel;
                if (_channelMap.TryGetValue(channelKey, out channel)) {
                    foreach (Action<TMessage> subscriber in channel) {
                        subscriber.Invoke(message);
                    }
                }
            }
            message.RemoveBlocker(this);
        }

        public void EnqueueMessage(TMessage message) {
            _queue.Enqueue(message);
        }
    }


    // A premade processor that auto-sorts processed messages by their runtime type
    //TODO -- add properties for ProcessAsSupertypes to allow post-constructor sorter-swapping?
    public class TypeFilteredMessageProcessor<TMessage> : AutoChannelMessageProcessor<TMessage, Type> where TMessage : IMessage{



        public TypeFilteredMessageProcessor(Queue<TMessage> queue, IDictionary<Type, ICollection<Action<TMessage>>> channelMap, bool processAsSuperTypes = false) :
            base(queue, channelMap, null ) {
            if (processAsSuperTypes) {
                this.Sorter = delegate (TMessage m) { return GetInheritanceTypes(m.GetType()); };
            } else {
                this.Sorter = delegate (TMessage m) { return new Type[] { m.GetType() }; };
            }
        }


        public static IEnumerable<Type> GetInheritanceTypes(Type type) {
            for (var current = type; current != null; current = current.BaseType)
                if (typeof(TMessage).IsAssignableFrom(current) ) {
                    yield return current;
                } else {
                    break;
                }
        }

    
    }



    //A message processor with 1 channel and no-arg callbacks.
    //Perhaps not super useful, but a good way to add queue/async handling even if you don't need filtering
    public class MessageProcessor<TMessage> : IMessageProcessor<TMessage> where TMessage : IMessage {

        Queue<TMessage> _queue;
        ICollection<Action<TMessage>> _channel;


        public MessageProcessor(Queue<TMessage> queue, ICollection<Action<TMessage>> channel){
            _queue = queue;
            _channel = channel;
	
		}



        public void FireMessage(TMessage message) {
            message.AddBlocker(this);
            foreach (Action<TMessage> subscriber in _channel) {
                subscriber.Invoke(message);

            }
            message.RemoveBlocker(this);
        }
       
    

		public void EnqueueMessage (TMessage message)
		{
            _queue.Enqueue(message);
		}

	}





	public class SubscriptionBroker<TMessage, TChannelKey> : ISubscriptionBroker<TMessage, TChannelKey> {

		//ICollection<Action<TMessage>> _channel;
		Action<Action<TMessage>, TChannelKey> _subscribeAction;
		Action<Action<TMessage>, TChannelKey> _unsubscribeAction;

		//IDictionary<object, event Action<TMessage>> coolChannels;


		/* The baseline constructor treats the filters directly as keys.
		 * If you want more nuanced behavior, use the other constructor 
		 *
		 */
		public SubscriptionBroker (IDictionary<TChannelKey, ICollection<Action<TMessage>>> channelMap){
			_subscribeAction = delegate(Action<TMessage> subscriber, TChannelKey key) {
				ICollection<Action<TMessage>> channel;
				if(channelMap.TryGetValue(key, out channel)){
					channelMap[key].Add (subscriber);
				};

			};

			_unsubscribeAction = delegate(Action<TMessage> subscriber, TChannelKey key) {
				ICollection<Action<TMessage>> channel;
				if(channelMap.TryGetValue(key, out channel)){
					channelMap[key].Remove (subscriber);
				};
			};

		}



		public SubscriptionBroker (Action<Action<TMessage>, TChannelKey> subscribeAction, Action<Action<TMessage>, TChannelKey> unsubscribeAction){
			_subscribeAction = subscribeAction;
			_unsubscribeAction = unsubscribeAction;

		}





		public void Subscribe (Action<TMessage> subscriber, TChannelKey key)
		{
			_subscribeAction.Invoke (subscriber, key);
		}

		public void Unsubscribe (Action<TMessage> subscriber, TChannelKey key)
		{
			_unsubscribeAction.Invoke (subscriber, key);
		}

	}


    /*
     * The MultimodeSubscriptionBroker provides both targeted and "catch-all" subscriptions.
     * 
     * 
     */ 
   // public class MultiModeSubscriptionBroker<TMessage, TChannelKey> : IMultiModeSubscriptionBroker<TMessage, TChannelKey> {

   //     //ICollection<Action<TMessage>> _channel;
   //     Action<Action<TMessage>, TChannelKey> _subscribeAction;
   //     Action<Action<TMessage>, TChannelKey> _unsubscribeAction;

   //     Action<Action<TMessage>> _catchAllSubscribeAction;
   //     Action<Action<TMessage>> _catchAllUnsubscribeAction;

   //     //IDictionary<object, event Action<TMessage>> coolChannels;


   //     /* The baseline constructor treats the filters directly as keys.
		 //* If you want more nuanced behavior, use the other constructor 
		 //*
		 //*/
   //     public MultiModeSubscriptionBroker(IDictionary<TChannelKey, ICollection<Action<TMessage>>> channelMap, ICollection<Action<TMessage>> catchAllChannel ) {
   //         _subscribeAction = delegate (Action<TMessage> subscriber, TChannelKey key) {
   //             ICollection<Action<TMessage>> channel;
   //             if (channelMap.TryGetValue(key, out channel)) {
   //                 channelMap[key].Add(subscriber);
   //             };

   //         };

   //         _unsubscribeAction = delegate (Action<TMessage> subscriber, TChannelKey key) {
   //             ICollection<Action<TMessage>> channel;
   //             if (channelMap.TryGetValue(key, out channel)) {
   //                 channelMap[key].Remove(subscriber);
   //             };
   //         };

   //         _catchAllSubscribeAction = delegate (Action<TMessage> subscriber) {
   //             catchAllChannel.Add(subscriber);
   //         };

   //         _catchAllUnsubscribeAction = delegate (Action<TMessage> subscriber) {
   //             catchAllChannel.Remove(subscriber);
   //         };

   //     }



   //     public MultiModeSubscriptionBroker(Action<Action<TMessage>, TChannelKey> subscribeAction, Action<Action<TMessage>, TChannelKey> unsubscribeAction, Action<Action<TMessage>> catchAllSubscribeAction, Action<Action<TMessage>> catchAllUnsubscribeAction ) {
   //         _subscribeAction = subscribeAction;
   //         _unsubscribeAction = unsubscribeAction;
   //         _catchAllSubscribeAction = catchAllSubscribeAction;
   //         _catchAllUnsubscribeAction = catchAllUnsubscribeAction;
   //     }





   //     public void Subscribe(Action<TMessage> subscriber, TChannelKey key) {
   //         _subscribeAction.Invoke(subscriber, key);
   //     }

   //     public void Unsubscribe(Action<TMessage> subscriber, TChannelKey key) {
   //         _unsubscribeAction.Invoke(subscriber, key);
   //     }

   //     public void Subscribe(Action<TMessage> subscriber) {
   //         _catchAllSubscribeAction.Invoke(subscriber);
   //     }

   //     public void Unsubscribe(Action<TMessage> subscriber) {
   //         _catchAllUnsubscribeAction.Invoke(subscriber);
   //     }

   // }







    public class Channel<T> : IChannel<T>{


		//The sub/super channel collections are lists becuase I'm assuming
		//they will either be tiny or largely static, so we don't need to worry
		//about O(n) removal ops. Subscribers are much more transient, hence the NodeList.
		List<IChannel<T>> _subChannels = new List<IChannel<T>> (); 
		ICollection<IChannel<T>> _superChannels = new List<IChannel<T>> ();
		ICollection<Action<T>> _subscribers = new NodeList<Action<T>> ();



		public ICollection<Action<T>> Subscribers {get {return _subscribers;}}
        public ReadOnlyCollection<IChannel<T>> SubChannels { get { return _subChannels.AsReadOnly(); } }

		public void AddSubChannel(IChannel<T> channel){
			if (!(_subChannels.Contains (channel))) {
				_subChannels.Add (channel);
				channel.AddSuperChannel (this);
			}

		}

		public void AddSuperChannel(IChannel<T> channel) {
			if (!(_superChannels.Contains (channel))) {
				_superChannels.Add (channel);
				channel.AddSubChannel (this);
			}
		}

		public void RemoveSubChannel(IChannel<T> channel) {
			if (_subChannels.Remove (channel)) {
				channel.RemoveSuperChannel (this);
			}
		}

		public void RemoveSuperChannel(IChannel<T> channel) {
			if (_superChannels.Remove (channel)) {
				channel.RemoveSubChannel (this);
			}
		}

	}




}
