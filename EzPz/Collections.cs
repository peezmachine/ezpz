﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace EzPz.Collections {

    namespace Observable {
        public class CollectionChangedEventArgs : EventArgs {
            public enum ChangeEventType { Add, Remove, Replace, Clear };
        }


        public class CollectionChangedEventArgs<T> : CollectionChangedEventArgs {
            //CONSIDFER -- should constructors flip out if they get the wrong kind of Type passed in?

            ChangeEventType _type;
            IList<T> _newItems;
            IList<T> _oldItems;
            int _newStartingIndex;
            int _oldStartingIndex;

            public ChangeEventType Type { get { return _type; } }
            public IList<T> NewItems { get { return _newItems; } }
            public IList<T> OldItems { get { return _oldItems; } }

            //no-index
            public CollectionChangedEventArgs(ChangeEventType type, IList<T> newItems, IList<T> oldItems) : base() {
                _type = type;
                _newItems = newItems;
                _oldItems = oldItems;
            }

            //one-item change (add/remove) no-index
            public CollectionChangedEventArgs(ChangeEventType type, T item) {
                _type = type;
                T[] items = new T[] { item };
                switch (type) {
                    case ChangeEventType.Add:
                        _newItems = items;
                        break;
                    case ChangeEventType.Remove:
                        _oldItems = items;
                        break;
                    default:
                        break;

                }
            }

            //one-item replace, no-index
            public CollectionChangedEventArgs(ChangeEventType type, T newItem, T oldItem) {
                _type = type;
                _newItems = new T[] { newItem };
                _oldItems = new T[] { oldItem };
            }

            //clear
            public CollectionChangedEventArgs(ChangeEventType type) {
                _type = type;
            }

            //single item add/remove with index
            public CollectionChangedEventArgs(ChangeEventType type, T item, int index) {
                _type = type;
                T[] items = new T[] { item };
                switch (type) {
                    case ChangeEventType.Add:
                        _newItems = items;
                        _newStartingIndex = index;
                        break;
                    case ChangeEventType.Remove:
                        _oldItems = items;
                        _oldStartingIndex = index;
                        break;
                    default:
                        break;

                }
            }

            //single item replace with index
            public CollectionChangedEventArgs(ChangeEventType type, T newItem, T oldItem,  int index) {
                _type = type;
                _newItems = new T[] { newItem };
                _oldItems = new T[] { oldItem };
                _oldStartingIndex = index;
                _newStartingIndex = index;
            }


            //group Add/remove, no index
            public CollectionChangedEventArgs(ChangeEventType type, IList<T> items) {
                _type = type;
                switch (type) {
                    case ChangeEventType.Add:
                        _newItems = items;
                        break;
                    case ChangeEventType.Remove:
                        _oldItems = items;
                        break;
                    default:
                        break;
                }
                
            }



        }


       // public delegate void CollectionChangedEventHandler<T>(CollectionChangedEventArgs<T> args);

        //IObservableCollection is pretty much just a minor INotifyCollectionChanged replacement 
        //Since EzPz was created to support Unity3D projects and Unity's Mono version did
        //not support that feature at the time
        public interface ICollectionChangedNotifier<T> {
            event EventHandler<CollectionChangedEventArgs<T>> OnCollectionChanged;
        }


        /* A friendly note to Unity3D users:
         * The EzPz observable list is NOT serializable by Unity.
         * To make a serialiazble version that you can use in the editor AND which
         * has a serialiazable backing field, you will need to reimplement ObservableList<T>
         * and then make non-generic subsclasses as is usually required for generic types.
         * 
         * Fortunately, re-implementing is easy! Just copy the existing EzPz class code into your project
         * and mark the "_list" field with [SerializeField]. Then you're free to subclass away.
         * 
         * Alternatively, you could just use a regular List<T> as a backing field and set that list
         * as tehe ObservableList's backer using the IList-taking constuctor. THIS IS PROBABLY SMARTER.
         * 
         */ 


        [Serializable]
        public class ObservableList<T> : IList<T>, ICollectionChangedNotifier<T> {

            List<T> _list;

            

            public int Count { get { return _list.Count; } }
            public bool IsReadOnly { get { return false; } } //I think? TODO -- Look into doing something with this

            event EventHandler<CollectionChangedEventArgs<T>> _onCollectionChanged = delegate { };

            public event EventHandler<CollectionChangedEventArgs<T>> OnCollectionChanged { add { _onCollectionChanged += value; } remove { _onCollectionChanged -= value; } }




            public ObservableList() {
                _list = new List<T>();
            }

            public ObservableList(List<T> backingList) {
                _list = backingList;
            }

            public ObservableList(int capacity) {
                _list = new List<T>(capacity);
            }






            public T this[int index] {
                get { return _list[index]; }
                set { //can only be used to replace existing items, can NOT add
                    T oldItem = _list[index];
                    _list[index] = value;
                    _onCollectionChanged.Invoke(this, new CollectionChangedEventArgs<T>(
                        CollectionChangedEventArgs.ChangeEventType.Replace,
                        value,
                        oldItem,
                        index
                     )
                );


                }
            }

            public void Add(T item) {
                _list.Add(item);
                _onCollectionChanged.Invoke(this,
                    new CollectionChangedEventArgs<T>(
                        CollectionChangedEventArgs.ChangeEventType.Add,
                        item,
                        _list.Count - 1
                    )
                );
            }

            public void Clear() {
                T[] oldItems = new T[_list.Count];
                _list.CopyTo(oldItems, 0);
                _list.Clear();
                _onCollectionChanged.Invoke(this,
                    new CollectionChangedEventArgs<T>(
                        CollectionChangedEventArgs.ChangeEventType.Clear
                    )
                );
            }

            public bool Contains(T item) {
                return _list.Contains(item);
            }

            public void CopyTo(T[] array, int arrayIndex) {
                _list.CopyTo(array, arrayIndex);
            }

            public IEnumerator<T> GetEnumerator() {
                return (_list as IEnumerable<T>).GetEnumerator();
            }

            public int IndexOf(T item) {
                return _list.IndexOf(item);
            }

          

            public void Insert(int index, T item) {
                _list.Insert(index, item);
                _onCollectionChanged.Invoke(this, new CollectionChangedEventArgs<T>(
                    CollectionChangedEventArgs.ChangeEventType.Add,
                    item,
                    index
                    )
                );
            }

            public bool Remove(T item) {
                int i = _list.IndexOf(item);
                if (i > -1) { //-1 is the not-found code for IndexOf
                    this.RemoveAt(i);
                    return true;
                } else {
                    return false;
                }
                
            }

            public void RemoveAt(int index) {
                T item = _list[index];
                _list.RemoveAt(index);
                _onCollectionChanged.Invoke(this, new CollectionChangedEventArgs<T>(
                    CollectionChangedEventArgs.ChangeEventType.Remove,
                    item,
                    index
                    )
                );
            }

            IEnumerator IEnumerable.GetEnumerator() {
                return _list.GetEnumerator();
            }
        }














        [Serializable]
        public class ObservableDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ICollectionChangedNotifier<KeyValuePair<TKey, TValue>> {

            IDictionary<TKey, TValue> _dictionary = new Dictionary<TKey, TValue>();


            event EventHandler<CollectionChangedEventArgs<KeyValuePair<TKey, TValue>>> _onCollectionChanged;

            public event EventHandler<CollectionChangedEventArgs<KeyValuePair<TKey, TValue>>> OnCollectionChanged { add { _onCollectionChanged += value; } remove { _onCollectionChanged -= value; } }



            //A way to essentially decorate some other dictionary implementation as observable;
            public ObservableDictionary(IDictionary<TKey, TValue> backer) {
                _dictionary = backer;
           }

            public ObservableDictionary(int capacity) {
                _dictionary = new Dictionary<TKey, TValue>(capacity);
            }



            #region IDictionary implementation
            public bool ContainsKey(TKey key) {
                return _dictionary.ContainsKey(key);
            }

            public void Add(TKey key, TValue value) {
                this.Add(new KeyValuePair<TKey, TValue>(key, value));
            }

            public bool Remove(TKey key) {
                TValue value;
                if (_dictionary.TryGetValue(key, out value) && _dictionary.Remove(key)) {
                    _onCollectionChanged.Invoke(
                        this,
                        new CollectionChangedEventArgs<KeyValuePair<TKey, TValue>>(
                            CollectionChangedEventArgs.ChangeEventType.Remove,
                            new KeyValuePair<TKey, TValue>(key, value) 
                        )
                    );
                    return true;
                } else {
                    return false;
                }

            }
            public bool TryGetValue(TKey key, out TValue value) {
                return _dictionary.TryGetValue(key, out value);
            }
            public TValue this[TKey key] {
                get {
                    return _dictionary[key];
                }
                set {
                    TValue oldValue;
                    if (_dictionary.TryGetValue(key, out oldValue)) {
                        //got an existing hit
                        _dictionary[key] = value;
                        _onCollectionChanged.Invoke(
                            this,
                            new CollectionChangedEventArgs<KeyValuePair<TKey, TValue>>(
                                CollectionChangedEventArgs.ChangeEventType.Replace,
                                new KeyValuePair<TKey, TValue>(key, value),
                                new KeyValuePair<TKey, TValue>(key, oldValue)
                            )
                        );

                    } else { // it's a new entry
                        this.Add(new KeyValuePair<TKey, TValue>(key, value));
                    }
                }
            }
            public ICollection<TKey> Keys {
                get {
                    return _dictionary.Keys;
                }
            }
            public ICollection<TValue> Values {
                get {
                    return _dictionary.Values;
                }
            }
            #endregion
            #region ICollection implementation
            public void Add(KeyValuePair<TKey, TValue> item) {
                //note that we are not checking for the key's prior existence because we are allowing
                //the key exception thrown by the backing Dictionary to bubble up
                _dictionary.Add(item);
                _onCollectionChanged.Invoke(
                    this,
                    new CollectionChangedEventArgs<KeyValuePair<TKey, TValue>>(
                        CollectionChangedEventArgs.ChangeEventType.Add,
                        item                    
                    )
                );

            }
            public void Clear() {
                _dictionary.Clear();
                _onCollectionChanged.Invoke(
                    this,
                    new CollectionChangedEventArgs<KeyValuePair<TKey, TValue>>(
                        CollectionChangedEventArgs.ChangeEventType.Clear
                    )
                );

            }
            public bool Contains(KeyValuePair<TKey, TValue> item) {
                return _dictionary.Contains(item);
            }
            public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex) {
                _dictionary.CopyTo(array, arrayIndex);
            }
            public bool Remove(KeyValuePair<TKey, TValue> item) {
                if (_dictionary.Remove(item)) {
                    _onCollectionChanged.Invoke(
                        this,
                        new CollectionChangedEventArgs<KeyValuePair<TKey, TValue>>(
                            CollectionChangedEventArgs.ChangeEventType.Remove,
                            item
                        )
                    );
                    return true;
                } else {
                    return false;
                }
            }
            public int Count {
                get {
                    return _dictionary.Count;
                }
            }
            public bool IsReadOnly {
                get {
                    return _dictionary.IsReadOnly;
                }
            }
            #endregion
            #region IEnumerable implementation
            public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator() {
                return _dictionary.GetEnumerator();
            }
            #endregion
            #region IEnumerable implementation
            IEnumerator IEnumerable.GetEnumerator() {
                return (_dictionary as IEnumerable).GetEnumerator();
            }
            #endregion
        }

        //Useful for indirection, when you want something to be able to respond to a collection's changes
        //without haveing access to the collection itself. For example, you might want a View to react
        //to changes to a collection in the Model without being able to modify it.
        public class CollectionChangedNotifier<T> : ICollectionChangedNotifier<T> {
            ICollectionChangedNotifier<T> _source;

            public event EventHandler<CollectionChangedEventArgs<T>> OnCollectionChanged { add { _source.OnCollectionChanged += value; } remove { _source.OnCollectionChanged -= value; } }

            public CollectionChangedNotifier(ICollectionChangedNotifier<T> source) {
                _source = source;
            }
        }


    }





    /*
     * DO NOT CHANGE THE NODELIST WHILE ITERATING OVER IT!
     * Doing this ^^ will invalidtate the iterator. This is part of documented IEnumerable/IEnumerator behavior.
     * Instead, just cache the nodes you want to remove, and do Remove(T[]) once you're done iterating.
     * 
     * 
     */


    // NodeList is essentially a Set + List

    public class NodeList : NodeList<object> {
        public enum KeyNode { Head, Tail }
    }

    

    [System.Serializable]
	public class NodeList<T> : IEnumerable<T>, ICollection<T> {

		

		#region fields

		Dictionary<T, Node<T>> _nodeMap; // for quick removal of a node, given the data payload
		Node<T> _head;
		Node<T> _tail;

		#endregion



        public T Head {
            get {
                if (_head != null) {
                    return _head.Contents;
                } else {
                    return default(T);
                }

            }
        }

        public T Tail {
            get {
                if (_tail != null) {
                    return _tail.Contents;
                } else {
                    return default(T);
                }

            }
        }


        public IEnumerator<T> GetEnumerator() {
			return new NodeEnumerator<T> (_head);
		}

		IEnumerator IEnumerable.GetEnumerator() {
			return this.nongenericEnumerator();
		}

		private IEnumerator nongenericEnumerator() {
			return this.GetEnumerator();
		}

		public int Count { get { return _nodeMap.Count; } }
		public bool IsReadOnly { get { return false; } }
		public void Clear() {
			_nodeMap.Clear ();
			_head = null;
			_tail = null;
		}
		public bool Contains(T item){
			return _nodeMap.ContainsKey (item);
		}
		public void CopyTo(T[] target, int i){
            throw new System.NotImplementedException();
			//return;
			//TODO -- actually implement this
		}






        public NodeList(int size) {
            _nodeMap = new Dictionary<T, Node<T>>(size);
        }

        public NodeList() {
			_nodeMap = new Dictionary<T, Node<T>> ();
		}

        public NodeList(IEnumerable<T> items) : this() {
            foreach(T item in items) {
                this.Add(item);
            }
        }


		public void Remove (IEnumerable<T> items){
			foreach (T item in items)
				this.Remove (item);
		}


		public bool Remove(T item) {
			Node<T> node;

			try {
				node = _nodeMap[item];
			} catch (KeyNotFoundException) {
				return false;
			}



			bool isHead = (node.Previous == null);
			bool isTail = (node.Next == null);

			if (isHead && isTail) { // head and tail
				_head = null;
				_tail = null;
			} else if (isHead) { // just head
				_head = node.Next;
				node.Next.Previous = null;
			} else if (isTail) { // just tail
				_tail = node.Previous;
				node.Previous.Next = null;
			} else { // neither head nor tail
				node.Next.Previous = node.Previous;
				node.Previous.Next = node.Next;
			}

			_nodeMap.Remove (item);
			return true;

		}

		//Add -- just a convience method for sticking a new node onto the tail
		public void Add(T item) {
			if (_nodeMap.Count != 0) {
				this.insertAfter (item, _tail);
			} else {
				Node<T> newNode = new Node<T> (item);
				_head = newNode;
				_tail = newNode;
				_nodeMap.Add (item, newNode);
			}
		}


        //TODO -- rename the arg to referenceItem
		public void Add(T newNode, T referenceNode, bool insertAfter = true) {
			switch (insertAfter) {
			case false:
				this.insertBefore (newNode, referenceNode);
				break;
			default:
				this.insertAfter (newNode, referenceNode);
				break;
			}
		}




		public void Add(T item, NodeList.KeyNode position) {
			Node<T> newNode = new Node<T> (item);
			if (_nodeMap.Count == 0) {
				_head = newNode;
				_tail = newNode;
				_nodeMap[item] = newNode;
				return;
			}


			switch (position) {
			case NodeList.KeyNode.Head:
				this.insertBefore (item, _head);
				break;
			case NodeList.KeyNode.Tail:
				this.insertAfter (item, _tail);
				break;
			default:
				break;
			}
		}


		protected void insertAfter(T item, T referenceItem) {
			Node<T> referenceNode = _nodeMap [referenceItem];
			this.insertAfter (item, referenceNode);
		}

		protected void insertAfter(T item, Node<T> referenceNode) {
			Node<T> newNode = new Node<T> (item);

			newNode.Next = referenceNode.Next;
			newNode.Previous = referenceNode;
			referenceNode.Next = newNode;



			if (referenceNode.Equals (_tail)) {
				_tail = newNode;
			} else {
				newNode.Next.Previous = newNode;
			}

			_nodeMap[item] = newNode;
		}

		 protected void insertBefore(T item, Node<T> referenceNode) {
			Node<T> newNode = new Node<T> (item);

			newNode.Next = referenceNode;
			newNode.Previous = referenceNode.Previous;
			referenceNode.Previous = newNode;


			if (referenceNode.Equals (_head)) {
				_head = newNode;
			} else { 
				newNode.Previous.Next = newNode;
			}

			_nodeMap[item] = newNode;
		}



		protected void insertBefore(T item, T referenceItem) {
			Node<T> referenceNode = _nodeMap [referenceItem];
			this.insertBefore (item, referenceNode);
		}




	}
    //TODO -- take the Node class private and redo the insert before / after stuff (remove the ref node ones)
	public class Node<T> {

		public T Contents;
		public Node<T> Next;
		public Node<T> Previous;
        

		public Node(T contents) {
			this.Contents = contents;
			this.Next = null;
			this.Previous = null;
		}
	}


	class NodeEnumerator<T> : IEnumerator<T>  {

		public NodeEnumerator(Node<T> head) {
			_head = head;
		}

		Node<T> _head;
		Node<T> _currentNode;

		public T Current { get { 
				if (_currentNode != null)
					return _currentNode.Contents; 

				throw new System.InvalidOperationException ();
			} }


		/*
	 * Important fun fact: When doing a foreach, the Enumerator calls MoveNext before starting.
	 * This means that you need to ensure that you start "in front" on the first of the first element
	 * (think C++ array addressing)
	 * 
	 */ 

		public bool MoveNext() {
			if (_currentNode == null) {
				if (_head != null) {
					_currentNode = _head;
					return true;
				} else {
					return false;
					//no head meand no list, get outta here!
				}
			}


			if (_currentNode.Next == null)
				return false;

			_currentNode = _currentNode.Next;
			return true;
		}

		public void Reset() {
			_currentNode = null;

		}


		public void Dispose(){
			
		}

		object IEnumerator.Current {
			get { return _currentNode.Contents; }
		}


	}

}







